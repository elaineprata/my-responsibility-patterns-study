package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.exception.CompensableException;

public class Step implements IterativeStep {

    private final Compensation compensation;

    public Step(Compensation compensation) {
        this.compensation = compensation;
    }

    public void perform() throws CompensableException {
        // TODO Auto-generated method stub
    }

	public Compensation getCompensation() {
	    return compensation;
	}
}
