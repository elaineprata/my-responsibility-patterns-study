package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.exception.CompensableException;

public interface IterativeStep {

    public void perform() throws CompensableException;

	public Compensation getCompensation();

}