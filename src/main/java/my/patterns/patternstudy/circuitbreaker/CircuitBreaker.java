package my.patterns.patternstudy.circuitbreaker;

import java.time.Duration;
import java.time.Instant;

/**
 * TODO multiple threads 
 *
 */
public class CircuitBreaker {
	
	static int MAX_ERRORS = 3;
	
	static int TIMEOUT = 5000;
	
	private SupplierConnector supplier;
	
	boolean circuitOpen = false;
	
	private int errors = 0;
	
	private Instant lastErrorInstant;
	
	public CircuitBreaker(SupplierConnector supplier) {
		this.supplier = supplier;
	}
	
	public void doSomething() throws SupplierConnectionException {
		
		if (!isCircuitOpen()) {
			try {
				this.supplier.doSomething();	
			} catch (SupplierConnectionException e) {
				recordError();
				throw e;
			}
		} else {
			throw new SupplierConnectionException();
		}
		
	}

	private void recordError() {
		increaseError();
	}
	
	private void increaseError() {
		this.errors++;
		
		this.circuitOpen = (this.errors >= MAX_ERRORS);
		if (this.circuitOpen) {
			this.lastErrorInstant = Instant.now();
		}
	}

	private boolean isCircuitOpen() {
		
		if (circuitOpen) {
			
			Duration timeElapsed = Duration.between(this.lastErrorInstant, Instant.now());
			if (timeElapsed.toMillis() >= TIMEOUT) {
				reset();
			}
		}
		return circuitOpen;
	}
	
	private void reset() {
		this.errors = 0;
		this.circuitOpen = false;
		this.lastErrorInstant = null;
	}
}