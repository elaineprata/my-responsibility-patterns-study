package my.patterns.patternstudy.circuitbreaker;

import java.util.Arrays;
import java.util.List;

public class MySupplierConnector extends SupplierConnector {
	
	private List<Boolean> throwExceptionList;

	private int index = 0;
	
	private int doSomethingCalls = 0;
	
	public MySupplierConnector(Boolean...throwException) {
		this.throwExceptionList = Arrays.asList(throwException);
	}

	@Override
	public void doSomething() throws SupplierConnectionException {

		doSomethingCalls++;
		if ((throwExceptionList != null) 
				&& (throwExceptionList.size() > index)
				&& (throwExceptionList.get(index))) {
			index++;
			throw new SupplierConnectionException();
		}
		index++;
	}

	public int getDoSomethingCalls() {
		return doSomethingCalls;
	}
}