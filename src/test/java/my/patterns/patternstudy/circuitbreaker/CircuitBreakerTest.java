package my.patterns.patternstudy.circuitbreaker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.Test;

public class CircuitBreakerTest {

	@Test
	public void test_doSomething_success_oneCall() throws Exception {
		
		MySupplierConnector supplierConnector = new MySupplierConnector(false);
		CircuitBreaker circuitBreaker = new CircuitBreaker(supplierConnector);
		
		circuitBreaker.doSomething();
		
		assertEquals(1, supplierConnector.getDoSomethingCalls());
		assertFalse(circuitBreaker.circuitOpen);
	}

	@Test
	public void test_doSomething_circuitOpenAfter3Errors_twoMoreCalls() throws Exception {
		
		MySupplierConnector supplierConnector = new MySupplierConnector(true, true, true);
		CircuitBreaker circuitBreaker = new CircuitBreaker(supplierConnector);
		
		assertFalse(circuitBreaker.circuitOpen);
		assertEquals(0, supplierConnector.getDoSomethingCalls());
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(1, supplierConnector.getDoSomethingCalls());
		}

		assertFalse(circuitBreaker.circuitOpen);
		
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(2, supplierConnector.getDoSomethingCalls());
		}

		assertFalse(circuitBreaker.circuitOpen);
		
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}
		
		assertTrue(circuitBreaker.circuitOpen);

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}
		
		assertEquals(3, supplierConnector.getDoSomethingCalls());
	}
	
	@Test
	public void test_doSomething_circuitOpenAfter3Errors_timeoutReset() throws Exception {
		
		MySupplierConnector supplierConnector = new MySupplierConnector(true, true, true);
		CircuitBreaker circuitBreaker = new CircuitBreaker(supplierConnector);
				
		assertEquals(0, supplierConnector.getDoSomethingCalls());
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(1, supplierConnector.getDoSomethingCalls());
		}

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(2, supplierConnector.getDoSomethingCalls());
		}

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}

		// this call will fail because the circuit is open
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}
		
		// timeout
		Thread.sleep(CircuitBreaker.TIMEOUT + 1000);
		
		try {
			circuitBreaker.doSomething();
			assertEquals(4, supplierConnector.getDoSomethingCalls());
		} catch (SupplierConnectionException e) {
			fail();
		}
		
	}

	@Test
	public void test_doSomething_circuitOpenAfter3Errors_timeoutReset_3MoreErrors() throws Exception {
		
		MySupplierConnector supplierConnector = new MySupplierConnector(true, true, true,
																		true, true, true);
		CircuitBreaker circuitBreaker = new CircuitBreaker(supplierConnector);
				
		assertEquals(0, supplierConnector.getDoSomethingCalls());
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(1, supplierConnector.getDoSomethingCalls());
		}

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(2, supplierConnector.getDoSomethingCalls());
		}

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}

		// this call will fail because the circuit is open
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(3, supplierConnector.getDoSomethingCalls());
		}
		
		// timeout
		Thread.sleep(CircuitBreaker.TIMEOUT + 1000);
		
		// but fail again 3 times
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(4, supplierConnector.getDoSomethingCalls());
		}
		
		assertFalse(circuitBreaker.circuitOpen);

		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(5, supplierConnector.getDoSomethingCalls());
		}
		
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(6, supplierConnector.getDoSomethingCalls());
		}
		
		assertTrue(circuitBreaker.circuitOpen);
		
		// this call will fail because the circuit is open
		try {
			circuitBreaker.doSomething();
			fail();
		} catch (SupplierConnectionException e) {
			assertEquals(6, supplierConnector.getDoSomethingCalls());
		}
		
		assertTrue(circuitBreaker.circuitOpen);
	}

}